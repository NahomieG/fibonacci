doubleTest(100000000)

function doubleTest(max) {
    var prev = timeTrial(2);
    console.log("#N    Time in Seconds");
    for(var N = 4; N < max; N+=1) {
      //get time
      var time = timeTrial(N);
      //time in seconds
      var s = time / 1000;
      //print the info
      console.log(`${N.toString().padStart(6, " ")} ${s}`);
      prev = time;
    }
  }

  function timeTrial(max) {
    var times = []
  
  //do the trial and do stopwatch things
    for(var i = 0; i < 5; i++) {
      var t1 = new Date();
      fibRecDP(max)
      var t2 = new Date();
      if(t1 > t2) {
          t2.setDate(t2.getDate() + 1);
      }
      var diff = t2.getTime() - t1.getTime();
      times[i] = t2 - t1;
    }
    return times.reduce((acc, c) => acc + c, 0) / times.length;
  }

 

function fibRecDP(num) {
    //define cache
    var cache = [];
    var ret;
    helper(num)
    function helper(n) {
        //if N is zero, return zero since it is the first number in the sequence
        if(n < 1) {
            ret = 0;
            return;
        //if N is less than or equal to two, return 1 since the next two numbers in the sequence are 1's
        } else if(n <= 2) {
            ret = 1;
            return;
        }

        //if passed in number is in the cache, return the number
        if(cache[n] != undefined) {
            ret = cache[n]
            return;
        }
        
        //recursively call fibRecDP to add the previous two numbers in the sequence and add to the cache
        cache[n] = fibRecDP(n - 1) + fibRecDP(n - 2);
        ret = cache[n];
        return;
    }
    return ret;
}

function fibRec(n) {
    //if N is zero, return zero since it is the first number in the sequence
    if(n < 1) {
        return 0;
    //if N is less than or equal to two, return 1 since the next two numbers in the sequence are 1's
    } else if(n <= 2) {
        return 1;
    }    
    
    //recursively call fibRecDP to find this sequence using the two previous values in the sequence
    return fibRec(n - 1) + fibRec(n - 2);
}

function fibLoop(n) {
    //if N is zero, return zero since it is the first number in the sequence
    if(n < 1) {
        return 0;
    //if N is less than or equal to two, return 1 since the next two numbers in the sequence are 1's
    } else if(n <= 2) {
        return 1;
    }  
    //initialize variables  
    var result = null;
    var cache = [];
    prev_a = 0;
    prev_b = 1;
    
    //start at two because we handled numbers 1-2 above
    //
    for(var i = 2; i < n + 1; i++) {
        //if this number is in the cache, then break and return it
        if(cache[i] != undefined) {
            result = cache[n]
            break;
        } else {
            //add the previous two numbers to the cache
            cache[i] = prev_a + prev_b;
            //increment a
            prev_a = prev_b;
            //set b to next value
            prev_b = cache[i]
            //finally set result if not found ever found in cache
            result = prev_b;
        }
    }
    
    return result;
}

// console.log(fibLoop(6))
// console.log(fibRec(6))
// console.log(fibRecDP(6))